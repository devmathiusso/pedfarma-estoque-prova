const mongoose = require("mongoose");
const AppErrorHandler = require("../utils/appErrorHandler");

const saleSchema = mongoose.Schema(
  {
    customer: {
      type: mongoose.Schema.ObjectId,
      ref: "Person",
      required: [true, "The sale must belong to a customer"]
    },
    products: [
      {
        _id: false,
        product: {
          type: mongoose.Schema.ObjectId,
          ref: "Product"
        },
        qty: {
          type: Number,
          default: 1
        }
      }
    ],
    cancelled: {
      type: Boolean,
      default: false
    },
    createdAt: {
      type: Date,
      default: Date.now()
    }
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
  }
);

// Calculating the total value of the sale
saleSchema.virtual("totalValue").get(function() {
  let totalVal = 0;
  const saleProducts = this.products;

  if (saleProducts.length > 0) {
    saleProducts.map(
      saleItem =>
        (totalVal += saleItem.product
          ? saleItem.product.price * saleItem.qty
          : 0)
    );
  }

  return totalVal;
});

saleSchema.pre(/^find/, function(next) {
  this.populate({
    path: "customer",
    select: "name cpf email"
  }).populate({
    path: "products.product",
    select: "name price referenceCode description provider slug"
  });

  next();
});

saleSchema.pre("save", function(next) {
  if (!this.products.length) {
    return next(
      new AppErrorHandler("The sale must have one or more products", 400)
    );
  }

  next();
});

const Sale = mongoose.model("Sale", saleSchema);

module.exports = Sale;
