const mongoose = require("mongoose");
const validator = require("validator");
const bcrypt = require("bcryptjs");

const userSchema = mongoose.Schema({
  email: {
    type: String,
    required: [true, "The user must have an email"],
    unique: true,
    trim: true,
    lowercase: true,
    validate: [validator.isEmail, "Please provide a valid email"]
  },
  role: {
    type: String,
    enum: ["admin", "user"],
    default: "user",
    lowercase: true
  },
  password: {
    type: String,
    required: [true, "Please, provide a password"],
    minlength: [8, "The password must have at least 8 characters"],
    select: false
  },
  passwordConfirm: {
    type: String,
    required: [true, "Please, confirm your password"],
    validate: {
      // This validation is called only on create and save
      validator: function(passConfirm) {
        return passConfirm === this.password;
      },
      message: "Passwords don't match"
    }
  },
  active: {
    type: Boolean,
    default: true,
    select: false
  }
});

// Hashing the password before save on the database
userSchema.pre("save", async function(next) {
  this.password = await bcrypt.hash(this.password, 12);
  this.passwordConfirm = undefined;

  next();
});

/**
 * providedPassword: The password provided by the user on login body request
 * userPassword: The actual user password
 */
userSchema.methods.checkIfPasswordIsCorrect = async function(
  providedPassword,
  userPassword
) {
  return await bcrypt.compare(providedPassword, userPassword);
};

const User = mongoose.model("User", userSchema);

module.exports = User;
