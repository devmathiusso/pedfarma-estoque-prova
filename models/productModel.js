const mongoose = require("mongoose");
const slugify = require("slugify");

const productSchema = mongoose.Schema({
  name: {
    type: String,
    required: [true, "The product must have a name"],
    trim: true
  },
  price: {
    type: Number,
    required: [true, "The product must contain a price"]
  },
  referenceCode: {
    type: String,
    required: [true, "The product must contain a reference code"],
    trim: true,
    unique: true
  },
  description: { type: String, trim: true },
  slug: String,
  provider: {
    type: mongoose.Schema.ObjectId,
    ref: "Person",
    required: [true, "The product must belong to a provider"]
  },
  active: {
    type: Boolean,
    default: true
  },
  createdAt: {
    type: Date,
    default: Date.now()
  }
});

// Slugifying the product name
productSchema.pre("save", function(next) {
  this.slug = slugify(this.name, { lower: true });
  next();
});

productSchema.pre(/^find/, function(next) {
  this.populate({
    path: "provider",
    select: "name cnpj email"
  });

  next();
});

const Product = mongoose.model("Product", productSchema);

module.exports = Product;
