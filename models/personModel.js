const mongoose = require("mongoose");
const validator = require("validator");
const { cpf, cnpj } = require("cpf-cnpj-validator");
const AppErrorHandler = require("../utils/appErrorHandler");
const catchAsync = require("../utils/catchAsync");

/**
 * As the Customer and Provider model is almost the same (the only thing that changes is the CPF and CNPJ fields)
 * I decided to merge these two models into this one
 */
const personSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, "The person name is required"],
      trim: true
    },
    personType: {
      type: String,
      required: [
        true,
        "The person type is required and has to be either PROVIDER or CUSTOMER"
      ],
      enum: ["PROVIDER", "CUSTOMER"],
      uppercase: true
    },
    cnpj: {
      type: String,
      required: [
        function() {
          // This field only has to be required when the person type is a provider
          return this.personType === "PROVIDER";
        },
        "The provider CNPJ is required"
      ],
      trim: true,
      //unique: true,
      validate: {
        /**
         * Validating provider CNPJ
         */
        validator: function(v) {
          if (this.personType === "PROVIDER") {
            // Avoiding to save CPF to a provider
            this.cpf = undefined;
            return cnpj.isValid(v);
          }
        },
        message: "The provider CNPJ is not valid"
      }
    },
    cpf: {
      type: String,
      required: [
        function() {
          // This field only has to be required when the person type is a customer
          return this.personType === "CUSTOMER";
        },
        "The customer CPF is required"
      ],
      trim: true,
      //unique: true,
      validate: {
        /**
         * Validating customer CPF
         */
        validator: function(v) {
          if (this.personType === "CUSTOMER") {
            // Avoiding to save CNPJ to a customer
            this.cnpj = undefined;
            return cpf.isValid(v);
          }
        },
        message: "The customer CPF is not valid"
      }
    },
    email: {
      type: String,
      required: [true, "The person email is required"],
      trim: true,
      unique: true,
      lowercase: true,
      validate: [validator.isEmail, "The person email is not valid"]
    },
    createdAt: {
      type: Date,
      default: Date.now()
    }
  },
  // For some reason, the collection name was saving as "people", so I had to force the collection name to "persons"
  { collection: "persons" }
);

/**
 * As the provider and customer is the same Model, I can't put the "cpf" or "cnpj" field
 * to be unique, because it would cause an error in the create operation, telling that
 * already exists someone with the "cpf" or "cnpj", even though this field
 * wasn't passed into the body of the request
 */
personSchema.pre("save", async function(next) {
  let findOneQueryObj = {};
  let fieldName = "";

  if (this.personType === "CUSTOMER") {
    findOneQueryObj.cpf = this.cpf;
    findOneQueryObj.personType = "CUSTOMER";
    fieldName = "cpf";
    this.cnpj = undefined;
  } else {
    findOneQueryObj.cnpj = this.cnpj;
    findOneQueryObj.personType = "PROVIDER";
    fieldName = "cnpj";
    this.cpf = undefined;
  }

  // Searching for someone with the same CPF or CNPJ depending on personType
  const personSameCpfOrCnpj = await Person.findOne(findOneQueryObj);

  /**
   * Throwing error if the above query found some customer with the same CPF
   * or some provider with the same CNPJ
   */
  if (personSameCpfOrCnpj) {
    next(
      new AppErrorHandler(
        `Duplicate field: ${fieldName.toUpperCase()}. Someone is already using this ${fieldName}. Please use another value!`,
        400
      )
    );
  }

  next();
});

const Person = mongoose.model("Person", personSchema);

module.exports = Person;
