const Provider = require("../models/personModel");
const catchAsync = require("../utils/catchAsync");
const AppErrorHandler = require("../utils/appErrorHandler");

const crudFactory = require("./crudFactory")(Provider, "providers", "provider");

// This message repeats a bunch of times on this controller
const defaultNotFoundMsg = "No results found with the provided ID";

exports.getProvider = crudFactory.findOne;

exports.getAllProviders = crudFactory.findAll;

exports.createProvider = crudFactory.createOne;

exports.updateProvider = crudFactory.updateOne;

exports.deleteProvider = crudFactory.deleteOne;
