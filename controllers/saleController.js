const Sale = require("../models/saleModel");
const catchAsync = require("../utils/catchAsync");
const AppErrorHandler = require("../utils/appErrorHandler");

const crudFactory = require("./crudFactory")(Sale, "sales", "sale");

// This message repeats a bunch of times on this controller
const defaultNotFoundMsg = "No results found with the provided ID";

exports.getSale = crudFactory.findOne;

exports.getAllSales = crudFactory.findAll;

exports.createSale = crudFactory.createOne;

exports.cancelSale = catchAsync(async (req, res, next) => {
  const sale = await Sale.findByIdAndUpdate(
    req.params.id,
    { cancelled: true },
    { new: true }
  );

  if (!sale) {
    return next(new AppErrorHandler(defaultNotFoundMsg, 404));
  }

  res.status(204).json({
    status: "success",
    data: null
  });
});
