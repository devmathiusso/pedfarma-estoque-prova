const Product = require("../models/productModel");
const catchAsync = require("../utils/catchAsync");
const AppErrorHandler = require("../utils/appErrorHandler");

const crudFactory = require("./crudFactory")(Product, "products", "product");

// This message repeats a bunch of times on this controller
const defaultNotFoundMsg = "No results found with the provided ID";

exports.getProduct = crudFactory.findOne;

exports.getAllProducts = crudFactory.findAll;

exports.createProduct = crudFactory.createOne;

/**
 * As this update is specific because of product slug, I won't use the CrudFactory
 *
 * The model.pre("save") hook, unfortunately, is not called into the .findOneAndUpdate()
 * or into the .findByIdAndUpdate(), so I had to use the .save() instead
 */
exports.updateProduct = catchAsync(async (req, res, next) => {
  const product = await Product.findById(req.params.id);

  if (!product) {
    return next(new AppErrorHandler(defaultNotFoundMsg, 404));
  }

  product.name = req.body.name;
  product.price = req.body.price;
  product.referenceCode = req.body.referenceCode;
  product.description = req.body.description;
  product.active = req.body.active;
  await product.save();

  res.status(200).json({
    status: "success",
    data: { product }
  });
});

exports.deleteProduct = crudFactory.deleteOne;
