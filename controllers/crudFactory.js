const catchAsync = require("../utils/catchAsync");
const AppErrorHandler = require("../utils/appErrorHandler");
const Sale = require("../models/saleModel");
const Product = require("../models/productModel");

// This message repeats a bunch of times on this controller
const defaultNotFoundMsg = "No results found with the provided ID";

/**
 * Factory created to handle all the CRUD operations,
 * in view of almost all the CRUD operations inside controllers is so much similar
 */
const init = (Model, pluralResKey, singularResKey) => {
  /**
   * Function created to insert "personType" position into the find object
   * in case of a request coming from "customers" or "providers"
   */
  const insertKeyObj = obj => {
    const modifiedObj = { ...obj };

    // Specifying the key to provider or customer
    if (singularResKey === "customer" || singularResKey === "provider") {
      modifiedObj.personType = singularResKey.toUpperCase();
    }

    return modifiedObj;
  };

  /**
   * Deletes Children that contains a document that is being deleted
   */
  const deleteChildrenDocuments = async (ChildrenModel, docId) => {
    const children = await ChildrenModel.find({ [singularResKey]: docId });

    /**
     * Removes all the children that belong to the document that is being deleted
     */
    if (children.length) {
      return await children.map(
        async child => await ChildrenModel.findByIdAndDelete(child._id)
      );
    }

    return;
  };

  /**
   * Find All
   */
  const findAll = catchAsync(async (req, res, next) => {
    const findObj = insertKeyObj({});

    const docs = await Model.find(findObj);

    res.status(200).json({
      status: "success",
      results: docs.length,
      data: { [pluralResKey]: docs }
    });
  });

  /**
   * Find One
   */
  const findOne = catchAsync(async (req, res, next) => {
    const findObj = insertKeyObj({ _id: req.params.id });

    const doc = await Model.findOne(findObj);

    if (!doc) {
      return next(new AppErrorHandler(defaultNotFoundMsg, 404));
    }

    res.status(200).json({
      status: "success",
      data: { [singularResKey]: doc }
    });
  });

  /**
   * Create
   */
  const createOne = catchAsync(async (req, res, next) => {
    const createObj = insertKeyObj({ ...req.body });

    const newDoc = await Model.create(createObj);

    res.status(201).json({
      status: "success",
      data: { [singularResKey]: newDoc }
    });
  });

  /**
   * Update
   */
  const updateOne = catchAsync(async (req, res, next) => {
    const findObj = insertKeyObj({ _id: req.params.id });
    const updateObj = insertKeyObj({ ...req.body });

    const doc = await Model.findOneAndUpdate(findObj, updateObj, {
      new: true,
      runValidators: true
    });

    if (!doc) {
      return next(new AppErrorHandler(defaultNotFoundMsg, 404));
    }

    res.status(200).json({
      status: "success",
      data: { [singularResKey]: doc }
    });
  });

  /**
   * Delete
   */
  const deleteOne = catchAsync(async (req, res, next) => {
    const findObj = insertKeyObj({ _id: req.params.id });

    const doc = await Model.findOneAndRemove(findObj);

    if (!doc) {
      return next(new AppErrorHandler(defaultNotFoundMsg, 404));
    }

    /**
     * Calls the function that removes children that belongs to the document being deleted
     *
     * In the case of "customer", will delete all the "sales" made from it
     * In the case of "provider", will delete all the "products" made from it
     */
    if (singularResKey === "customer") {
      await deleteChildrenDocuments(Sale, doc._id);
    } else if (singularResKey === "provider") {
      await deleteChildrenDocuments(Product, doc._id);
    }

    res.status(204).json({
      status: "success",
      data: null
    });
  });

  return { findAll, findOne, createOne, updateOne, deleteOne };
};

module.exports = init;
