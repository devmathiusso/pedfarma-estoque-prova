const AppErrorHandler = require("../utils/appErrorHandler");

const handleCastErrorDatabase = err => {
  const message = `Invalid ${err.path}: ${err.value}`;
  return new AppErrorHandler(message, 400);
};

const handleDuplicateFieldsDatabase = err => {
  /**
   * Getting the duplicated field name
   *
   * Errmsg example:
   *  E11000 duplicate key error collection: stock-pedfarma.persons index: cpf_1 dup key: { : null }
   */
  const fieldName = err.errmsg
    .split("index:")[1]
    .split("dup key")[0]
    .split("_")[0];

  const message = `Duplicate field:${fieldName.toUpperCase()}. Someone is already using this${fieldName}. Please use another value!`;
  return new AppErrorHandler(message, 400);
};

const handleValidationErrorDatabase = err => {
  const errors = Object.values(err.errors).map(el => el.message);

  const message = `Invalid input data. ${errors.join(". ")}`;
  return new AppErrorHandler(message, 400);
};

const handleJwtError = () =>
  new AppErrorHandler("Invalid token. Please, log in again", 401);

const handleJwtExpiredError = () =>
  new AppErrorHandler("Your token has expired. Please, log in again", 401);

const sendErrorEnvDev = (err, res) => {
  res.status(err.statusCode).json({
    status: err.status,
    error: err,
    message: err.message,
    stack: err.stack
  });
};

const sendErrorEnvProd = (err, res) => {
  if (err.isOperational) {
    // Operational: errors thrown by me
    res.status(err.statusCode).json({
      status: err.status,
      message: err.message
    });
  } else {
    // Not operational: Programming or other unknow error

    res.status(500).json({
      status: "error",
      message: "Something went wrong... Try again later!"
    });
  }
};

module.exports = (err, req, res, next) => {
  err.statusCode = err.statusCode || 500;
  err.status = err.status || "error";

  if (process.env.NODE_ENV === "development") {
    // Sending error with more details when the environment is development
    sendErrorEnvDev(err, res);
  } else if (process.env.NODE_ENV === "production") {
    let error = { ...err, message: err.message };

    if (error.name === "CastError") {
      error = handleCastErrorDatabase(error);
    }

    // Mongo returns error code as 11000 when refers to duplicate values for unique fields
    if (error.code === 11000) {
      error = handleDuplicateFieldsDatabase(error);
    }

    // Treating errors referring to invalid values to certain fields into the database
    if (error.name === "ValidationError") {
      error = handleValidationErrorDatabase(error);
    }

    // Handle invalid and general JWT errors
    if (error.name === "JsonWebTokenError") {
      error = handleJwtError();
    }

    // Handle the expired token error
    if (error.name === "TokenExpiredError") {
      error = handleJwtExpiredError();
    }

    // Sending error without complex details when the environment is production
    sendErrorEnvProd(error, res);
  }
};
