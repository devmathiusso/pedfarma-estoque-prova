const Customer = require("../models/personModel");
const catchAsync = require("../utils/catchAsync");
const AppErrorHandler = require("../utils/appErrorHandler");

const crudFactory = require("./crudFactory")(Customer, "customers", "customer");

// This message repeats a bunch of times on this controller
const defaultNotFoundMsg = "No results found with the provided ID";

exports.getCustomer = crudFactory.findOne;

exports.getAllCustomers = crudFactory.findAll;

exports.createCustomer = crudFactory.createOne;

exports.updateCustomer = crudFactory.updateOne;

exports.deleteCustomer = crudFactory.deleteOne;
