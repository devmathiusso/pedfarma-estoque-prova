const { promisify } = require("util");
const jwt = require("jsonwebtoken");
const User = require("../models/userModel");
const catchAsync = require("../utils/catchAsync");
const AppErrorHandler = require("../utils/appErrorHandler");

// Register the user's tokens
const signToken = id => {
  return jwt.sign({ id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN
  });
};

const createAndSendToken = (user, statusCode, res) => {
  const token = signToken(user._id);

  // Removing user password from the output
  user.password = undefined;

  res.status(statusCode).json({
    status: "success",
    token,
    data: { user }
  });
};

// The signup route will automatically log in the user if everything runs well
exports.signup = catchAsync(async (req, res, next) => {
  // Get only necessary data from body request
  const { name, email, password, passwordConfirm, role } = req.body;

  const newUser = await User.create({
    name,
    email,
    password,
    passwordConfirm,
    role
  });

  createAndSendToken(newUser, 201, res);
});

exports.login = catchAsync(async (req, res, next) => {
  // Get only necessary data from body request
  const { email, password } = req.body;

  // Checks if the body request got the email and password position filled
  if (!email || !password) {
    return next(
      new AppErrorHandler("Please provide your email and password"),
      400
    );
  }

  // Check if the user exists
  // Forces the "password" field to come back in the JSON, since this field is not selected as default
  const user = await User.findOne({ email }).select("+password");

  // If there's no user found or the password is incorrect, throw an error
  // Goes inside User model and call a function that checks if the password is correct
  if (
    !user ||
    !(await user.checkIfPasswordIsCorrect(password, user.password))
  ) {
    return next(new AppErrorHandler("Incorrect email or password", 401));
  }

  createAndSendToken(user, 200, res);
});

/**
 * Middleware created to protect some routes from not logged in users
 */
exports.protect = catchAsync(async (req, res, next) => {
  const { authorization } = req.headers;
  let token;

  // Getting the token and checking if it starts with "Bearer", the default JWT
  if (authorization && authorization.startsWith("Bearer")) {
    // Getting token itself without the "Bearer" part
    token = authorization.split(" ")[1];
  }

  if (!token) {
    return next(
      new AppErrorHandler(
        "You are not logged in! Please, log in to get access...",
        401
      )
    );
  }

  // Verify if the token is valid and decode it
  const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET);

  // Get the user referring to the token
  const currentUser = await User.findById(decoded.id);

  // Grant access to protected route and insert the current user on the request
  req.user = currentUser;
  next();
});

/**
 * Middleware created to restrict some routes to specific type of user,
 * For example, the "Delete Customer" route, it'll be allowed only for administrators
 */
exports.restrictTo = (...roles) => {
  return (req, res, next) => {
    // Checks if the user role is included to the allowed roles
    if (!roles.includes(req.user.role)) {
      return next(
        new AppErrorHandler("You're not allowed to access this route", 403)
      );
    }

    next();
  };
};
