const mongoose = require("mongoose");
const dotenv = require("dotenv");

// Handling global exceptions untreated
process.on("uncaughtException", err => {
  console.log("Uncaught exception caught! Shutting down...");
  console.log(err.name, ":", err.message);
  process.exit(1);
});

dotenv.config({ path: "./config.env" });
const app = require("./app");

// Replacing the PASSWORD piece of string by the database password
const db = process.env.DATABASE.replace(
  "<PASSWORD>",
  process.env.DATABASE_PASSWORD
);

// Try connection to the database
mongoose
  .connect(db, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
  })
  .then(() => console.log("DB Connection Successful!"));

const port = process.env.PORT || 3000;

// Initializing server
app.listen(port, () => {
  console.log(`App running on port: ${port}`);
});

// Handling global unhandled rejections
process.on("unhandledRejection", err => {
  console.log("Unhandled rejection caught! Shutting down...");
  console.log(err.name, ":", err.message);
  server.close(() => {
    process.exit(1);
  });
});
