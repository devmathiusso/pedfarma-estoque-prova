# Projeto de Estoque - Processo Seletivo da PedFarma

## Instalação do projeto

1. Clonar o projeto utilizando `git clone`.

2. Instalar packages utilizando `yarn` ou `npm install`

3. Inicializar o servidor

> Simulação do ambiente de **Desenvolvimento**:

```
yarn start
```

> Simulação do ambiente de **Produção**:

```
yarn start:prod
```

## Feito com

- [Node.js](https://nodejs.org)
- [Express](https://expressjs.com/)
- [MongoDB](https://www.mongodb.com/)

## Packages adicionais

- [mongoose](https://mongoosejs.com/)
- [bcryptjs](https://github.com/dcodeIO/bcrypt.js#readme)
- [dotenv](https://github.com/motdotla/dotenv#readme)
- [jsonwebtoken](https://github.com/auth0/node-jsonwebtoken)
- [morgan](https://github.com/expressjs/morgan)
- [slugify](https://github.com/simov/slugify)
- [validator](https://github.com/validatorjs/validator.js)
- [express-rate-limit](https://github.com/nfriedly/express-rate-limit)
- [helmet](https://github.com/helmetjs/helmet)
- [express-mongo-sanitize](https://github.com/fiznool/express-mongo-sanitize#readme)
- [xss-clean](https://github.com/jsonmaur/xss-clean)
- [cpf-cnpj-validator](https://github.com/carvalhoviniciusluiz/cpf-cnpj-validator#readme)

## Versionamento

Utilizei [Git](https://git-scm.com/) com [GitFlow](https://nvie.com/posts/a-successful-git-branching-model/) para o controle de versão.

## Autor

- **Victor Mathiusso**
