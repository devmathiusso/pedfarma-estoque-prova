const express = require("express");
const morgan = require("morgan");

const rateLimit = require("express-rate-limit");
const helmet = require("helmet");
const mongoSanitize = require("express-mongo-sanitize");
const xss = require("xss-clean");

const AppErrorHandler = require("./utils/appErrorHandler");
const globalErrorHandler = require("./controllers/errorController");
const authController = require("./controllers/authController");

// Importing routes
const authRoutes = require("./routes/authRoutes");
const customerRoutes = require("./routes/customerRoutes");
const providerRoutes = require("./routes/providerRoutes");
const productRoutes = require("./routes/productRoutes");
const saleRoutes = require("./routes/saleRoutes");

// Initializing express
const app = express();

// Set security HTTP headers
app.use(helmet());

// Development logging
if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}

// Allow 100 requests from the same IP in a window of 1 hour
const limiter = rateLimit({
  max: 100,
  windowMs: 60 * 60 * 1000,
  message: "Too many requests from this IP. Please try again in an hour!"
});

app.use("/api", limiter);

// Allowing JSON into the body request
app.use(express.json());

// Data sanitization against NoSQL query injection
app.use(mongoSanitize());

// Data sanitization against XSS
app.use(xss());

// Initializing routes
const endpointPrefix = "/api/v1";
app.use(`${endpointPrefix}`, authRoutes);
/**
 * Routes below protected to logged in users only
 */
app.use(`${endpointPrefix}/customers`, authController.protect, customerRoutes);
app.use(`${endpointPrefix}/providers`, authController.protect, providerRoutes);
app.use(`${endpointPrefix}/products`, authController.protect, productRoutes);
app.use(`${endpointPrefix}/sales`, authController.protect, saleRoutes);

// Preventing unfriendly error to undefined routes
app.all("*", (req, res, next) => {
  next(
    new AppErrorHandler(`Can't find ${req.originalUrl} on this server!`, 404)
  );
});

// Middleware to return errors thrown by the AppErrorHandler as JSON response
app.use(globalErrorHandler);

module.exports = app;
