const express = require("express");
const saleController = require("../controllers/saleController");

const router = express.Router();

router
  .route("/")
  .get(saleController.getAllSales)
  .post(saleController.createSale);

router.route("/:id").get(saleController.getSale);

router.route("/:id/cancel").patch(saleController.cancelSale);

module.exports = router;
