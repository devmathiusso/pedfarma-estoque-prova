/**
 * Class created to handle all the application errors
 *
 * For example, if a customer was not found by the ID, this class
 * will be called and throw the error
 *
 * All this to avoid the repetitive way to throw an error to the response
 */
class AppErrorHandler extends Error {
  constructor(message, statusCode) {
    super(message);

    this.statusCode = statusCode;
    this.status = `${statusCode}`.startsWith("4") ? "fail" : "error";
    this.isOperational = true; // That means that I throw the error manually

    Error.captureStackTrace(this, this.constructor);
  }
}

module.exports = AppErrorHandler;
