/**
 * Function that wraps async functions and catch async errors
 *
 * Specially created to avoid try/catch blocks inside async functions
 */
module.exports = fn => {
  return (req, res, next) => {
    fn(req, res, next).catch(next);
  };
};
